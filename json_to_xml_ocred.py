#!/usr/bin/env python
#
#     json_to_xml_ocred.py
#
# Get COBHUNI json source files from ocred texts and convert text and metadata into xml format
#
# usage:
#   $ python json_to_xml_ocred.py
#
##############################################################################################

import os
import sys
import ujson as json
import argparse
from bs4 import BeautifulSoup
from configparser import ConfigParser

import util


#
# Constants
#

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

config = ConfigParser(inline_comment_prefixes=('#'))
config.read(os.path.join(CURRENT_PATH, 'config.ini'))

OCRED_INPATH = os.path.join(CURRENT_PATH, config.get('json paths', 'ocred files'))
OCRED_OUTPATH = os.path.join(CURRENT_PATH, config.get('xml paths', 'ocred files'))


#
# Functions
#

def create_ocred_xml(text):
    """ Create xml structure for ocred texts.

    Args:
        text (str): Content to add to xml file.

    Returns:
       str: Xml string.

    """
    soup = BeautifulSoup(features='xml')

    soup.append(soup.new_tag('a'))
    soup.a.append(soup.new_tag('content'))
    soup.a.content.append(text)

    return soup.prettify()
    

#
# Main
#

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='convert annotated json files from ocred texts into xml')
    args = parser.parse_args()

    print('\n====== Converting files from ocred...\n', file=sys.stderr)

    for fpath,fname in util.get_files(OCRED_INPATH):

        with open(fpath) as fp:
                
            print('**', fname, file=sys.stderr)
            text = json.load(fp)['text']

        with open(os.path.join(OCRED_OUTPATH, '%s.xml' % fname), 'w') as outfp:
            print(create_ocred_xml(text), file=outfp)

