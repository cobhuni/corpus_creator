Raw Corpus Creator
==================

Create annis corpus with all text files from COBHUNI along with its metadata. The output data can be found in *COBHUNI/development/corpus/visualization/raw/data/annis*. There are three corpora: ocred_texts, altafsir and hadith_alislam.

## Usage

```sh
$ make
