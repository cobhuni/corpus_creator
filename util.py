#!/usr/bin/env python
#
#     util.py
#
# Utility functions
#
#################################################

import os

def get_files(path):
    """ Generate list of file paths and names from directory path.

    Args:
       path (str): Input directory

    Yields:
        str,str: full path of file and name

    """
    fobjs = (f for f in os.scandir(path) if f.is_file())

    for f in fobjs:
        yield (f.path, os.path.splitext(f.name)[0])

