#!/bin/bash
#
#     convert_workflows.sh
#
# Execute conversion from xml into annis with pepper using a genericXml reader.
#
# +--------------------------------------------------------------------------------------------+
# |                                        WARNING                                             |
# | Be aware that pepper cannot be executed by a script from inside the COBHUNI remote server. |
# | This is a problem of rights that cannot be changed. So a copy of the pepper software must  |
# | be copied into a local directory and the variable PEPPER_DIR must be updated accordingly.  |
# +--------------------------------------------------------------------------------------------+
#
# usage:
#   $ bash convert_worflows.sh
#
################################################################################################

PEPPER_DIR="/home/alicia/Desktop/pepper_tool/pepper/"

WORKFLOW_OCRED=$(echo $PWD)/"convert_ocred.pepper"
WORKFLOW_ALTAFSIR=$(echo $PWD)/"convert_altafsir.pepper"
WORKFLOW_HADITH=$(echo $PWD)/"convert_hadith.pepper"

echo -e                                               1>&2
echo -e " +----------------------------------------+" 1>&2
echo -e " |    Starting xml to Annis convertion    |" 1>&2
echo -e " +----------------------------------------+" 1>&2

cd "$PEPPER_DIR"

bash pepperStart.sh "$WORKFLOW_OCRED"
bash pepperStart.sh "$WORKFLOW_ALTAFSIR"
bash pepperStart.sh "$WORKFLOW_HADITH"
