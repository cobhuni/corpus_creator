#!/usr/bin/env python
#
#     json_to_xml_hadith.py
#
# Get COBHUNI json source files from hadith al-islam and convert text and metadata into xml format
#
# usage:
#   $ python json_to_xml_hadith.py
#
##################################################################################################

import os
import sys
import ujson as json
import argparse
import itertools
from bs4 import BeautifulSoup
from configparser import ConfigParser

import util


#
# Constants
#

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

config = ConfigParser(inline_comment_prefixes=('#'))
config.read(os.path.join(CURRENT_PATH, 'config.ini'))

HADITH_INDEXPATH = os.path.join(CURRENT_PATH, config.get('json paths', 'hadith indexes file'))
HADITH_INPATH = os.path.join(CURRENT_PATH, config.get('json paths', 'hadith files'))
HADITH_OUTPATH = os.path.join(CURRENT_PATH, config.get('xml paths', 'hadith files'))


#
# Functions
#

def get_indexes(path):
    """ Generate index dictionaries along with their id.

    Get only commentaries, i.e. from id 33 to 39.

    Args:
        path (str): Directory containing indexes.

    Yields:
        str, dict: Id and structure of index.

    """
    for fpath,fname in util.get_files(path):

        indexId = fname.partition('.')[0].partition('_')[2]

        if indexId in ('33', '34', '35', '36', '37', '38', '39'):

            with open(fpath) as fp:
                indexObj = json.load(fp)
            
                yield indexId, indexObj

def create_hadith_xml(metadata, text, source_type, book_name_key):
    """ Create xml structure for hadith al-islam texts and metadata
        and associate source_type attribute to it.

    Args:
        metadata (dict): Metadata to add to xml structure.
        text (str): Content to add to xml file.
        source_type (str): Value of source attribute. It can only be "original" or "exegesis".
        book_name_key (str): Key from metadata for taking the name of the collection.

    Returns:
       str: Xml string.

    """
    # create xml output
    soup = BeautifulSoup(features='xml')
    soup.append(soup.new_tag('a'))

    name = metadata[book_name_key]
    soup.a.append(soup.new_tag('b', collection=name, source=source_type, href=metadata['url']))
    soup.a.b.append(soup.new_tag('c', book=metadata['chapter']))

    if metadata['subchapter']:
        soup.a.b.c.append(soup.new_tag('d', chapter=metadata['subchapter']))

    if metadata['section']:
        soup.a.b.c.d.append(soup.new_tag('e', section=metadata['section']))

    soup.a.append(soup.new_tag('content'))
    soup.a.content.append(text)

    return soup.prettify()
    

#
# Main
#

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='convert source json files into xml')
    args = parser.parse_args()

    print('\n====== Converting files from hadith.al-islam...\n', file=sys.stderr)
    
    # read all index files for exegesis texts
    indexes = dict(get_indexes(HADITH_INDEXPATH))
            
    # get all corpus files
    for fpath,fname in util.get_files(HADITH_INPATH):
    
        with open(fpath) as fp:
            print('**', fname, file=sys.stderr)
    
            dataobj = json.load(fp)
            
            original_text = dataobj['original']
            exegesis_text = dataobj['commentary']
    
            # prepare metadata with ids
            metadata = dict(itertools.zip_longest(('pidstart', 'pidend', 'book', 'chapter', 'subchapter', 'section'),
                                              itertools.chain(*(i.split('-') for i in fname.split('-',2)[-1].split('_')))))
    
            # get indexes from metadata to parse index structures
            bookid = metadata['book']
            chapterid = int(metadata['chapter'])
            if metadata['subchapter']:
                subchapterid = int(metadata['subchapter'])
    
            # add url info for exegesis. create only the start page
            metadata['url'] = 'http://hadith.al-islam.com/Page.aspx?pageid=192&BookID=%s&PID=%s' % \
                                   (metadata['book'], metadata['pidstart'])
                
            metadata['book original'] = config.get('book id names original', metadata['book'])
            metadata['book exegesis'] = config.get('book id names exegesis', metadata['book'])
                
            # replace ids for title names in metadata
            metadata['chapter'] = indexes[bookid][chapterid]['name']
    
            if metadata['subchapter']:
                metadata['subchapter'] = indexes[bookid][chapterid]['subchapters'][subchapterid]['name']
    
            if metadata['section']:
                metadata['section'] = indexes[bookid][chapterid]['subchapters'][subchapterid]['sections']\
                                                                      [int(metadata['section'])]['name']
    
            # convert to xml and save
            with open(os.path.join(HADITH_OUTPATH, '%s_original.xml' % fname), 'w') as ori_outfp, \
                 open(os.path.join(HADITH_OUTPATH, '%s_exegesis.xml' % fname), 'w') as exe_outfp:
    
                print(create_hadith_xml(metadata, original_text, 'original', 'book original'), file=ori_outfp)
                print(create_hadith_xml(metadata, exegesis_text, 'exegesis', 'book exegesis'), file=exe_outfp)

