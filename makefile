
OCRED_INPUT=../../data/xml/ocred_texts/*.xml
ALTAFSIR_INPUT=../../data/xml/altafsir/*.xml
HADITH_INPUT=../../data/xml/hadith_alislam/*.xml

OCRED_OUTPUT=../../data/annis/ocred_texts/
ALTAFSIR_OUTPUT=../../data/annis/altafsir/
HADITH_OUTPUT=../../data/annis/hadith_alislam/

RM=/bin/rm -f
PYTHON=/usr/bin/env python

.PHONY : all convert_xml convert_annis clean help

all: clean convert_xml convert_annis

convert_xml:
	$(PYTHON) json_to_xml_ocred.py
	$(PYTHON) json_to_xml_altafsir.py
	$(PYTHON) json_to_xml_hadith.py

convert_annis:
	bash convert_workflows.sh

clean:
	$(RM) $(OCRED_INPUT)
	$(RM) $(ALTAFSIR_INPUT)
	$(RM) $(HADITH_INPUT)
	$(RM) -r $(OCRED_OUTPUT)
	$(RM) -r $(ALTAFSIR_OUTPUT)
	$(RM) -r $(HADITH_OUTPUT)
	cp ocred_texts.meta ../../data/xml/ocred_texts/
	cp altafsir.meta ../../data/xml/altafsir/
	cp hadith_alislam.meta ../../data/xml/hadith_alislam/

help:
	@echo "    clean"
	@echo "        Remove intermediate xml files"
	@echo "    convert_xml"
	@echo "        Convert annotated json files into xml"
	@echo "    convert_annis"
	@echo "        Convert xml files into annis"
	@echo "    all"
	@echo "        Clean xml files and execute conversions"
